// Reccursive script to dive into folders with FLAC files
// and generate md5 checksums files.
// Useful for music albums with FLAC files that needs some sort of integrity control file.

package main

import (
	"crypto/md5"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

// ChecksumFile - exported file for md5 sums
const ChecksumFile = "checksum.md5"

// FileExtension - only this extension will be md5'ed
const FileExtension = ".flac"

type hashFilePair struct {
	SourceFile string
	Hash       []byte
}

func run(path string) {
	fmt.Println("Generating md5 checksums for", path)

	err := filepath.Walk(path,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if info.IsDir() {
				results, err := processFiles(path)
				if err != nil {
					return err
				}

				saveChecksums(path, results)
			}

			return nil
		})

	if err != nil {
		log.Fatal(err)
	}
}

func processFiles(path string) (results []hashFilePair, err error) {
	files, err := filepath.Glob(path + "/*" + FileExtension)
	if err != nil {
		return results, err
	}

	for _, file := range files {
		hash, err := calculateHash(file)
		if err != nil {
			return results, err
		}
		results = append(
			results,
			hashFilePair{SourceFile: filepath.Base(file), Hash: hash})
	}

	return results, nil
}

func calculateHash(file string) (hash []byte, err error) {
	f, err := os.Open(file)
	if err != nil {
		return hash, err
	}
	defer f.Close()

	hasher := md5.New()
	_, err = io.Copy(hasher, f)
	if err != nil {
		return hash, err
	}

	hash = hasher.Sum(nil)

	return hash, nil
}

func saveChecksums(path string, results []hashFilePair) error {
	if len(results) == 0 {
		return nil
	}

	checksumPath := filepath.Join(path, ChecksumFile)

	checksumFile, err := os.Create(checksumPath)
	if err != nil {
		return err
	}
	defer checksumFile.Close()

	for _, data := range results {
		line := fmt.Sprintf("%s %x", data.SourceFile, data.Hash)

		_, err = fmt.Fprintln(checksumFile, line)
		if err != nil {
			log.Fatal(err)
		}
	}

	fmt.Printf("Checksum file generated: %s\n", checksumPath)

	return nil
}

func main() {
	if os.Args == nil || len(os.Args) <= 1 {
		fmt.Println("Specify the root path as the first parameter")
		fmt.Println("e.g. ./checksum_generator /mnt/music/FLAC")
		os.Exit(0)
	}

	var verFlag = flag.Bool("version", false, "Version of the script")

	flag.Parse()

	if *verFlag {
		fmt.Println("checksum version 1.0 Nov-2020")
		os.Exit(0)
	}

	path := os.Args[1]

	run(path)
}
